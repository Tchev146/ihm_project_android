package com.kevinkerboit.smartsudoku;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


public class JeuActivity extends AppCompatActivity {

    Intent intent;
    Grille grille;
    String grille_string;
    int x, y;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        intent = getIntent();
        grille_string = intent.getStringExtra("grille");
        grille = findViewById(R.id.grille);
        grille.set(grille_string);
        grille.invalidate();
    }

    public void onClickValider(View v){
        Boolean result = grille.gagne();
        grille.setGagne(result);
        grille.invalidate();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    //------------------------------------------------
//---------- Code pour l'activity Choix ----------
//------------------------------------------------
//    public void startActiviteChoix(int x, int y){
//        int rc = 0;
//        this.x = x;
//        this.y = y;
//        startActivityForResult(intent, rc);
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
//        System.out.println("startActiviteChoix");
//        if(requestCode == 0){
//            System.out.println("startActiviteChoix requestCode");
//            if(resultCode == Activity.RESULT_OK){
//                String result = intent.getStringExtra("result");
//                System.out.println("startActiviteChoix = "+result);
//
//                grille.set(x, y, Integer.parseInt(result));
//            }
//        }
//
//    }
//------------------------------------------------
//------------------------------------------------

}
