package com.kevinkerboit.smartsudoku;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class Grille extends View implements View.OnTouchListener {

    private int screenWidth;
    private int screenHeight;
    private int n;

    private Boolean gagne = null;

    private int Casex ,Casey;

    private Paint paint1;   // Pour dessiner la grille (lignes noires)
    private Paint paint2;   // Pour le texte des cases fixes
    private Paint paint3;   // Pour dessiner les lignes rouges (grosse)
    private Paint paint4;   // Pour le texte noir des cases a modifier
    private Paint paintGagne;
    private Paint paintPerdu;

    private int[][] matrix = new int[9][9];
    private boolean[][] fixIdx = new boolean[9][9];

    public Grille(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Grille(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Grille(Context context) {
        super(context);
        init();
    }


    private void init() {
        //Grille de depart
        //set("000105000140000670080002400063070010900000003010090520007200080026000035000409000");

        // Grille Gagnante
        //set("672145398145983672389762451263574819958621743714398526597236184426817935831459267");

        // Grille Perdante
        //set("672145198145983672389762451263574819958621743714398526597236184426817935831459267");

        paint1 = new Paint();
        paint1.setAntiAlias(true);
        paint1.setColor(Color.BLACK);
        paint1.setStyle(Paint.Style.STROKE);

        paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(Color.RED);
        paint2.setTextSize((float) 40);
        paint2.setTextAlign(Paint.Align.CENTER);

        paint3 = new Paint();
        paint3.setAntiAlias(true);
        paint3.setColor(Color.RED);
        paint3.setStrokeWidth(8);

        paint4 = new Paint();
        paint4.setAntiAlias(true);
        paint4.setColor(Color.BLACK);
        paint4.setTextSize((float) 40);
        paint4.setTextAlign(Paint.Align.CENTER);

        paintGagne = new Paint();
        paintGagne.setAntiAlias(true);
        paintGagne.setColor(Color.GREEN);
        paintGagne.setStrokeWidth(20);
        paintGagne.setStyle(Paint.Style.STROKE);

        paintPerdu = new Paint();
        paintPerdu.setAntiAlias(true);
        paintPerdu.setColor(Color.RED);
        paintPerdu.setStrokeWidth(20);
        paintPerdu.setStyle(Paint.Style.STROKE);

        setOnTouchListener(this);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        screenWidth = getWidth();
        screenHeight = getHeight();
        int w = Math.min(screenWidth, screenHeight);
        w = w - (w%9);
        n = w / 9 ;

        int tailleCases = 0;

        // Dessiner w lignes verticles et w lignes horizontales noires
       for(int i = 0; i < w; i+=n){
           for(int j = 0; j < w; j+=n){
               canvas.drawRect(i+tailleCases, j+tailleCases , i+n-tailleCases, j+n-tailleCases, paint1);
           }
       }

        // Dessiner 2 lignes rouges verticales et 2 lignes rouges horizontales
        for(int j = (n*3); j < w; j+=(n*3)){
            canvas.drawLine(j, 0, j, w, paint3);
            canvas.drawLine(0, j, w, j, paint3);
        }

        // Les contenus des cases
        String s;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                s = "" + (matrix[j][i] == 0 ? "" : matrix[j][i]);
                if (fixIdx[j][i])
                    canvas.drawText(s, i * n + (n / 2) - (n / 10), j * n
                            + (n / 2) + (n / 10), paint2);
                else
                    canvas.drawText(s, i * n + (n / 2) - (n / 10), j * n
                            + (n / 2) + (n / 10), paint4);
            }
        }

        if(gagne != null){
            if(gagne == true){
                canvas.drawRect(0, 0, w, w, paintGagne);
            }
            else{
                canvas.drawRect(0, 0, w, w, paintPerdu);
            }
        }


    }

    public int getXFromMatrix(int x) {
        // Renvoie l'indice d'une case a partir du pixel x de sa position h

        return (x / n);
    }

    public int getYFromMatrix(int y) {
        // Renvoie l'indice d'une case a partir du pixel y de sa position v

        return (y / n);
    }

    public void set(String s, int i) {
        // Remplir la ieme ligne de la matrice matrix avec un vecteur String s
        int v;
        for (int j = 0; j < 9; j++) {
            v = s.charAt(j) - '0';
            matrix[i][j] = v;
            if (v == 0)
                fixIdx[i][j] = false;
            else
                fixIdx[i][j] = true;
        }
    }

    public void set(String s) {
        // Remplir la matrice matrix a partir d'un vecteur String s

        if(s == null || s.equals("")){
            return;
        }

        for (int i = 0; i < 9; i++) {
            set(s.substring(i * 9, i * 9 + 9), i);
        }
    }

    public void set(int x, int y, int v) {
        // Affecter la valeur v a la case (y, x)
        // y : ligne
        // x : colonne

        if(isNotFix(x, y)){
            matrix[y][x] = v;
            this.invalidate();
        }

    }

    public boolean isNotFix(int x, int y) {
        // Renvoie si la case (y, x) n'est pas fixe

        boolean result = true;

        if(fixIdx[y][x]){
            result = false;
        }
        return result;
    }

    public boolean gagne() {
        // Verifier si la case n'est pas vide ou bien s'il existe
        // un numero double dans chaque ligne ou chaque colonne de la grille
        for (int v = 1; v <= 9; v++) {
            for (int i = 0; i < 9; i++) {
                boolean bx = false;
                boolean by = false;
                for (int j = 0; j < 9; j++) {
                    if (matrix[i][j] == 0) return false;
                    if ((matrix[i][j] == v) && bx) return false;
                    if ((matrix[i][j] == v) && !bx) bx=true;
                    if ((matrix[j][i] == v) && by) return false;
                    if ((matrix[j][i] == v) && !by) by=true;
                }
            }
        }
        // ------
        // Gagne
        return true;
    }

    public boolean onTouch(View v, MotionEvent event){

        int action = event.getAction();
        int x, y;

        if(action == MotionEvent.ACTION_DOWN){
            x = (int) event.getX();
            y = (int) event.getY();

            Casex = getXFromMatrix(x);
            Casey = getXFromMatrix(y);

            if(isNotFix(Casex, Casey) == true) {
                JeuActivity host = (JeuActivity) v.getContext();

//------------------------------------------------
//---------- Code pour l'activity Choix ----------
//------------------------------------------------
//                host.startActiviteChoix(Casex,Casey);
//------------------------------------------------
//------------------------------------------------

                AlertDialog.Builder builder = new AlertDialog.Builder(host);
                builder.setTitle("Choisissez une valeur");
                builder.setItems(R.array.listView_Values,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                set(Casex, Casey, which);
                            }
                        });
                builder.create().show();
            }
        }


        return true;
    }

    public void setGagne(boolean bool){
        this.gagne = bool;
    }
}
