package com.kevinkerboit.smartsudoku;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void play(View v){
        Intent myIntent = new Intent(this, ChoixGrilleActivity.class);
        startActivity(myIntent);
    }

    public void about(View v){
        Intent myIntent = new Intent(this, AboutActivity.class);
        startActivity(myIntent);
    }
}
