package com.kevinkerboit.smartsudoku;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ChoixGrilleActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ListView list_view;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_grille);
        list_view = findViewById(R.id.list_view);
        list_view.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id){

        String url_string = "http://pagesperso.univ-brest.fr/~bounceur/android/tpsudoku/sw/index.php?v=";
        String grille_string = "";

        int choix = 0;

        if(position==0){
            choix = (int) (Math.random() * 29);
        }
        else{
            choix = position - 1;
        }

        url_string = url_string.concat(""+choix);

        GetMethodDemo request = new GetMethodDemo();

        try {
            grille_string = request.execute(url_string).get();
            Log.i("TAG", ""+grille_string);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        intent= new Intent(this, JeuActivity.class);
        intent.putExtra("grille", grille_string);
        startActivity(intent);

    }
}
