package com.kevinkerboit.smartsudoku;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ChoixActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ListView list_view;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix);
        intent = getIntent();
        list_view = findViewById(R.id.list_view);
        list_view.setOnItemClickListener((AdapterView.OnItemClickListener) this);

    }

    @Override
    public void onItemClick(AdapterView<?> adapter, View view, int position, long id){
        String s = (String) list_view.getItemAtPosition(position);
        intent.putExtra("result", s);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
